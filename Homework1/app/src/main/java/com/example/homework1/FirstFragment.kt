package com.example.homework1

import android.icu.lang.UCharacter.getAge
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homework1.databinding.FragmentFirstBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    fun getWeight():Double{
        var value = binding.editTextWeight.text.toString().toDouble()
        return if(value in 0.0..250.0)
            value
        else -1.0
    }

    private var userWeight: Double
        get() {
            return getWeight()
        }
        set(value: Double) {}

    fun getHeight():Int{
        var value = binding.editTextHeight.text.toString().toInt()
            return if(value in 0..250)
                value
            else -1
        }

    private var userHeight: Int
        get() {
            return getHeight()
        }
        set(value: Int) {}

    fun getAge():Int{
        var value = binding.editTextAge.text.toString().toInt()
        return if(value in 0..150)
            value
        else -1
    }

    private var userAge: Int
        get() {
            return getAge()
        }
        set(value: Int) {}

    private var userName: String
        get() {
            return getName()
        }
        set(value: String) {
        }
    fun getName():String{
        var value = binding.editTextName.text.toString()
        return if(value.length<=50)
            value
        else ""
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonCalculate.setOnClickListener {
            var wrongData = GetWrongData(userName, userHeight, userWeight, userAge )
            if(wrongData!="")
                binding.textViewResult.text = "Incorrect $wrongData"
            else{
                var BMI = (userWeight/((userHeight.toDouble()/100)*(userHeight.toDouble()/100)))
                BMI = (BMI*100).toInt().toDouble()/100//я знаю, что это кошмар, но нормальных способов округления в джаве не нашел
                var comparedToNormal = if (BMI>25) "more than normal"
                else if(BMI<18) "less than normal" else "normal"
                binding.textViewResult.text ="$userName, your BMI is equal $BMI, which is $comparedToNormal. " +
                        "At the age of $userAge, you " +
                        if(comparedToNormal!="normal") "should be more careful about your health"
                        else "are doing great!"
            }
        }
    }
    fun GetWrongData(name:String, height:Int, weight:Double, age:Int): String {
        if(name == "")return "name"
        if(height == -1) return "height"
        if(weight == -1.0) return "weight"
        if(age== -1)return "age"
        return ""
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}