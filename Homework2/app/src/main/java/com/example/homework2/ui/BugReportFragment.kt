package com.example.homework2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

class BugReportFragment : Fragment(){
    var bugSource:String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val data = arguments?.getString("ARG_TEXT").orEmpty()
        if(data.isNotEmpty()&&view!=null)
            Snackbar.make(view!!, data, Snackbar.LENGTH_LONG)
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}