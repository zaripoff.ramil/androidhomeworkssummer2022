package com.example.homework2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.homework2.R
import com.example.homework2.databinding.FragmentHomeBinding
import com.google.android.material.snackbar.Snackbar

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.textHome.text = "Oh no, there is nothing! Submit bugreport so we force our developer do some stuff here"
        binding.bugReportButton.setOnClickListener {
            Snackbar.make(view!!, "Sent report", Snackbar.LENGTH_LONG)
            /*var controller = findNavController()
            var bundle = Bundle()
            bundle.putString("ARG_TEXT",
                controller.currentDestination?.label.toString())
            controller.navigate(R.id.navigation_bug_report, bundle)*/

        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}